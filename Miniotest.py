import cv2
from minio import Minio
from datetime import datetime, date
import io, uuid
import urllib.request
import numpy as np

minioClient = Minio( "147.46.61.150:9000", access_key="admin", secret_key="Abcd@123", secure=False)

bucket = "test"
base_url_storage = "http://147.46.61.150:9000/test/"

def uploadimage():
	img= cv2.imread("/home/muoi/Work/Counting_Recognition/face_recognition_counting/Gallery/San_1.jpg")

	date_time_minio = date.today().strftime("%Y%m%d")
	name= uuid.uuid4().hex
	filename = date_time_minio + "/" + name+"_img.jpg"

	img_as_bytes = cv2.imencode(".jpg", img)[1]
	img_as_stream = io.BytesIO(img_as_bytes)
	minioClient.put_object(bucket, filename, img_as_stream, len(img_as_bytes))
	url_img= base_url_storage + filename
	print ( base_url_storage + filename )
	return url_img

def download_image(url):
    """Download image from image url to opencv type """
    
    try:
        resp = urllib.request.urlopen(url, timeout=0.5)
    except:
        resp = urllib.request.urlopen(url, timeout=0.5)

    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image
if __name__ == '__main__':
	url_img = uploadimage()
	cv2.namedWindow('xxxx', cv2.WINDOW_NORMAL)
	cv2.resizeWindow('xxxx', 1440, 810)
	image = download_image(url_img)
	cv2.imshow("xxxx", image)
	cv2.waitKey(0)